# Android Tools Golang

Android Tools Golang. Es una capa para la ejecucionde comandos Termux.

## Required Packages for Android:
- Terminal
    * [Termux"](https://play.google.com/store/apps/details?id=com.termux&hl=es_419)
- Terminal Api:
    * [ Termux Api](https://play.google.com/store/apps/details?id=com.termux.api&hl=es_VE)
    * [ Termux Api How to install](https://wiki.termux.com/wiki/Termux:API)

## Simple Example 
 
```go
package main

// GOARCH=arm go build -o androidBin
// run in termux for android './androidBin'

import (
	"fmt"

	"gitlab.com/AndrusGerman/androidtools"
)

func main() {
	// Comands Android
	var com = new(androidtools.AndroidComands)
	com.Call("04248580544")
	com.Sms("04248580544", "¿Hola como estas?")
	com.Comand("mkdir NewFolder")
	// Battery
	var battery = new(androidtools.Battery)
	battery.Get()
	fmt.Println(battery.Health)
	fmt.Println(battery.Temperature)
	fmt.Println(battery.Percentage)
	// Location
	var location = new(androidtools.Location)
	location.Get()
	fmt.Println(location.Latitude)
	fmt.Println(location.Longitude)
	fmt.Println(location.Altitude)

}

```
### Compilation:
* Run `GOARCH=arm go build -o AppAndroid`

