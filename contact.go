package androidtools

import (
	"encoding/json"
)

// Contact data
type Contact []struct {
	Name   string `json:"name"`
	Number uint   `json:"number"`
}

// GetAll Contact list
func (ctx *Contact) GetAll() (dato *Contact, err error) {
	data, err := Comand("termux-contact-list").Output()
	if err != nil {
		return ctx, err
	}
	err = json.Unmarshal(data, ctx)
	return ctx, err
}
