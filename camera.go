package androidtools

// Camera camera comands
type Camera struct {
}

// Photo Start photo
func (ctx *Camera) Photo(name string) error {
	_, err := Comand("termux-camera-photo " + name).Output()
	return err
}

// PhotoAndOpen Start photo and open
func (ctx *Camera) PhotoAndOpen(name string) (err error) {
	_, err = Comand("termux-camera-photo " + name).Output()
	if err != nil {
		return err
	}
	_, err = Comand("termux-open " + name).Output()
	return err
}
