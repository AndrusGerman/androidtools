package androidtools

import (
	"encoding/json"
)

// Location data
type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Altitude  float32 `json:"altitude"`
	Accuracy  string  `json:"accuracy"`
	Bearing   string  `json:"bearing"`
	Speed     float32 `json:"speed"`
	ElapsedMs uint    `json:"elapsedMs"`
	Provider  string  `json:"provider"`
}

// Get Data
func (ctx *Location) Get() (err error) {
	data, err := Comand("termux-location").Output()
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, ctx)
	return err
}
