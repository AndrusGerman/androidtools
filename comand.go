package androidtools

import (
	"os/exec"
)

// Comand in android
func Comand(comand string) *exec.Cmd {
	return exec.Command("/data/data/com.termux/files/usr/bin/bash", "-c", comand)
}
