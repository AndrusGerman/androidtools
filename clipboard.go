package androidtools

// Clipboard camera comands
type Clipboard struct {
}

// Set clipboard data
func (Clipboard) Set(data string) error {
	_, err := Comand("termux-clipboard-set '" + data + "'").Output()
	return err
}

// Get clipboard data
func (Clipboard) Get() string {
	data, _ := Comand("termux-clipboard-get").Output()
	return string(data)
}
