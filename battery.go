package androidtools

import (
	"encoding/json"
)

// Battery data
type Battery struct {
	Health      string  `json:"health"`
	Percentage  uint    `json:"percentage"`
	Plugged     string  `json:"plugged"`
	Status      string  `json:"status"`
	Temperature float32 `json:"temperature"`
}

// Get Data
func (ctx *Battery) Get() (err error) {
	data, err := Comand("termux-battery-status").Output()
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, ctx)
	return err
}
