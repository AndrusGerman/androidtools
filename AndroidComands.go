package androidtools

import (
	"fmt"
)

// AndroidComands Clase de comandos termux
type AndroidComands struct{}

// Comand ejecuta un comando
func (AndroidComands) Comand(comando string) (string, error) {
	cmd := Comand(comando)
	resp, err := cmd.Output()
	return string(resp), err
}

// Sms llama al numero pasado por parametro
func (ctx AndroidComands) Sms(numero string, msg string) error {
	_, err := Comand(fmt.Sprintf("termux-sms-send -n %s '%s'", numero, msg)).Output()
	return err
}

// Call llama al numero pasado por parametro
func (ctx AndroidComands) Call(numero string) error {
	_, err := Comand(fmt.Sprintf("termux-telephony-call %s", numero)).Output()
	return err
}
